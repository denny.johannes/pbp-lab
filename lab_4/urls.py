from django.urls import path

from praktikum import settings
from .views import index, add_note, note_list
from django.conf.urls.static import static

urlpatterns = [
    path('', index, name='index'),
    path('add-note',add_note, name='add note'),
    path('note-list', note_list, name='note list'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)