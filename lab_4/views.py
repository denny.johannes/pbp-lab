from django.shortcuts import render
from .models import Note
from .forms import NoteForm
# Create your views here.
def index(request):
    li = Note.objects.all()
    response = {'li':li}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context={}
    form = NoteForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
    context['form']=form
    return render(request, 'lab4_form.html',context)

def note_list(request):
    li = Note.objects.all()
    response = {'li':li}
    return render(request, 'lab4_note_list.html', response)
