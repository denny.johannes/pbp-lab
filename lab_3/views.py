from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import FriendForm
from .models import Friend
# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    # TODO Implement this
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
@login_required(login_url="/admin/login/")
def add_friend(request):
    context={}
    form = FriendForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect("/lab-3")
    context['form']=form
    return render(request, 'lab3_form.html',context)

