from django.shortcuts import render
from datetime import datetime, date
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers
# Create your views here.
def index(request):
    li = Note.objects.all()
    response = {'li':li}
    return render(request, 'index_lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
