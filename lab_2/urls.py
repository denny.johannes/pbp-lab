from django.urls import path
from .views import index
from . import views


urlpatterns = [
    path('', index, name='index'),
    path('xml', views.xml, name='index_lab2'),
    path('json', views.json, name='index_lab2')
]